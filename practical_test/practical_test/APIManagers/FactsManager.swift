//
//  CategoryManager.swift
//  practical_test
//
//  Created by Maulik on 25/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation

class FactsManager {
    
    static let shared = FactsManager()
    let session = URLSession.shared
    
    func getFactList(success:@escaping(FactResponseEntity?)-> Void,failure:@escaping (_ error: NSError )-> Void) {
        let factsURL = URL(string:factURL)
        
        guard let url = factsURL else { return }
        
        let task = session.dataTask(with: url) { (data, response, error) in
            
            guard data != nil,
                error == nil else { return }
            
            if let responseData = response as? HTTPURLResponse {
                if responseData.statusCode == 200 {
                    guard let string = String(data: data!, encoding: String.Encoding.isoLatin1) else { return }
                    
                    guard let formatedData = string.data(using: .utf8, allowLossyConversion: true) else { return }
                    
                    let factObj = try? JSONDecoder().decode(FactResponseEntity.self, from: formatedData)
                    
                    if factObj != nil {
                        success(factObj)
                    }
                } else {
                    failure(error! as NSError)
                }
            } else {
                failure(error! as NSError)
            }
        }
        task.resume()
    }
}
