//
//  StructConstant.swift
//  practical_test
//
//  Created by Maulik on 25/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation

struct RefreshControl {
    static let title = "Refreshing data..."
}

struct Alert {
    
    struct Title {
        static let noConnectivity = "Connectivity issue"
    }
    
    struct Message {
        static let noConnectivity = "Internet connectivity is requried. Please connect your phone with Mobile Data OR WiFi."
    }
    
    struct Button {
        static let ok = "Ok"
    }
}


