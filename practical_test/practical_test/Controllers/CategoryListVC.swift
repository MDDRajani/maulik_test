//
//  CategoryListVC.swift
//  practical_test
//
//  Created by Maulik on 24/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import UIKit

class FactsListVC: UIViewController {
    
    // UIControls
    let tableView = UITableView()
    
    override func loadView() {
        super.loadView()
        setupTableView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // check for connectivity
        if Reachability.isConnectedToNetwork() {
            configureUI()
        } else {
            print("Internet Connection not Available!")
            // show alert
            Helper.shared.showAlert(title: alert.title.noConnectivity, message: alert.message.noConnectivity, viewcontroller: self, actions: [UIAlertAction(title: alert.button.ok, style: .cancel, handler: nil)])
        }
    }
}


private extension FactsListVC {
    func configureUI() {
        
        title = "Facts List"
        view.backgroundColor = .white
        
        DispatchQueue.main.async {
            self.configureData()
        }
    }
    
    func setupTableView() {
        // set frame, delegate and dataSource
        
        view.addSubview(tableView)
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
    }
    
    func configureData() {
        // fetch data request
        
        
        let session = URLSession.shared
        let url = URL(string: "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")
        
        let task = session.dataTask(with: url, completionHandler: { data, response, error in
            print(data)
            print(response)
            print(error)
        })
        

        task.resume()
    }
}
