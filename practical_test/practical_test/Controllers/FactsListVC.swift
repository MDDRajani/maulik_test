//
//  CategoryListVC.swift
//  practical_test
//
//  Created by Maulik on 24/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import UIKit

let defaultHeight:CGFloat = 70.0

class FactsListVC: UIViewController {

    // UIControls
    let tableView = UITableView() //  for Data container
    
    var refreshControl = UIRefreshControl() // pull to refresh
    
    var factObj: FactResponseEntity? // store api response object
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // check for connectivity
        if !Reachability.isConnectedToNetwork() {
            // show alert
            Helper.shared.showAlert(title: Alert.Title.noConnectivity, message: Alert.Message.noConnectivity, viewcontroller: self, actions: [UIAlertAction(title: Alert.Button.ok, style: .cancel, handler: nil)])
        }
        configureUI()
    }
}

// MARK: - Configure Screen
private extension FactsListVC {
    func configureUI() {
        
        title = "Facts List"
        view.backgroundColor = .white
        setupTableView()
        self.fetchData()
    }
    
//    MARK: - Set tableview
    func setupTableView() {
        // set frame, delegate and dataSource
        
        view.addSubview(tableView)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        // Remove extra separators
        tableView.tableFooterView = UIView()
        
        //constraints for tableView
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        tableView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        // register cell
        self.tableView.register(FactTableViewCell.self, forCellReuseIdentifier: String(describing: FactTableViewCell.self))
        
        // configure and add refresh control
        refreshControl.attributedTitle = NSAttributedString(string: RefreshControl.title)
        refreshControl.addTarget(self, action: #selector(self.fetchData), for: .valueChanged)
        tableView.addSubview(refreshControl)
    }
    
//    MARK: - FetchData
    @objc func fetchData() {
        if !Reachability.isConnectedToNetwork() {
            // reset internal object and show alert
            factObj = nil // set the object to 'nil' as its not having latest data. We are deal with the latest data else keep as it is.
            reloadTable()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                Helper.shared.showAlert(title: Alert.Title.noConnectivity, message: Alert.Message.noConnectivity, viewcontroller: self, actions: [UIAlertAction(title: Alert.Button.ok, style: .cancel, handler: nil)])
            })
            return
        }
        
        FactsManager.shared.getFactList(success: {[weak self] (factObject) in
            guard let fact = factObject else { return }
            self?.factObj = fact
            DispatchQueue.main.async {
                self?.setData(factObject)
            }
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func setData(_ factDetail: FactResponseEntity?) {
        self.navigationItem.title = factDetail?.title // update title from response
        reloadTable()
    }
    
    func reloadTable() {
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
}

// MARK: - UITableViewDataSource

extension FactsListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return factObj?.rows?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: FactTableViewCell.self), for: indexPath) as! FactTableViewCell
        cell.configureUI(factObj!.rows![indexPath.row])
        return cell
    }
}

// MARK: - UITableViewDelegate

extension FactsListVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let fact = factObj?.rows?[indexPath.row]
        if fact?.description == "" || fact?.description == nil {
            return defaultHeight
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
