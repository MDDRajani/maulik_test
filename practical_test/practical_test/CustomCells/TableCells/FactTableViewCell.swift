//
//  FactTableViewCell: UI.swift
//  practical_test
//
//  Created by Maulik on 25/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import UIKit
import Kingfisher

class FactTableViewCell: UITableViewCell {
    
    var fact: FactEntity?

    
    // show image from url
    let thumbImgView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
        imgView.clipsToBounds = true
        return imgView
    }()
    
    // title  label
    let lblTitle: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize:18)
        lbl.numberOfLines = 0
        
        return lbl
    }()
    
    // description  label
    let lblDescription: UILabel = {
        
        let lbl = UILabel()
        lbl.textColor = .darkGray
        lbl.font = UIFont.systemFont(ofSize:14)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        
        return lbl
    }()
    
    
//    MARK: - ConfigureUI
    override func configureUI(_ model: Any, indexpath: IndexPath? = nil, viewController: UIViewController? = nil) {
        
        fact = model as? FactEntity
        
        thumbImgView.removeFromSuperview()
        lblTitle.removeFromSuperview()
        lblDescription.removeFromSuperview()
        
        addSubview(thumbImgView)
        addSubview(lblTitle)
        addSubview(lblDescription)
        
        thumbImgView.kf.setImage(with: URL(string: fact?.imageHref ?? ""), placeholder: UIImage(named: "iconPlaceholer"), options: nil, progressBlock: nil) // downloading image from url if available
        lblTitle.text = fact?.title
        lblDescription.text = fact?.description
        
        thumbImgView.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 0, width: 50, height: 50, enableInsets: false)
        
        lblTitle.anchor(top: topAnchor, left: leftAnchor, bottom: nil, right: rightAnchor, paddingTop: 5, paddingLeft: 60, paddingBottom: 0, paddingRight: 0, width: frame.size.width - 80, height: 0, enableInsets: false)
        
        lblDescription.anchor(top: lblTitle.bottomAnchor, left: lblTitle.leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 20, paddingRight: 5, width: lblTitle.frame.size.width, height: 0, enableInsets: false)
    }
}
