//
//  FactEntity.swift
//  practical_test
//
//  Created by Maulik on 25/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import UIKit

struct FactEntity: Decodable {
    let title: String?
    let description: String?
    let imageHref: String?
}
