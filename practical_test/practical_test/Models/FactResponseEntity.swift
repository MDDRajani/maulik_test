//
//  FactResponseEntity.swift
//  practical_test
//
//  Created by Maulik on 25/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

struct FactResponseEntity: Decodable {

    let title: String?
    let rows: [FactEntity]?
}
