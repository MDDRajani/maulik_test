//
//  Helper.swift
//  practical_test
//
//  Created by Maulik on 25/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import Foundation
import UIKit

class Helper {
    
    static let shared = Helper()
    
    func showAlert(title: String? = nil, message : String? = nil, viewcontroller : UIViewController, actions: [UIAlertAction]) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        for action in actions {
            alertView.addAction(action)
        }
        
        viewcontroller.present(alertView, animated: true, completion: nil)
    }
    
}
