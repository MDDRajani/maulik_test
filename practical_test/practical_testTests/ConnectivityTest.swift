//
//  ConnectivityTest.swift
//  practical_testTests
//
//  Created by Maulik on 26/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import XCTest

@testable import practical_test

class ConnectivityTest: XCTestCase {
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    override class func setUp() {
        super.setUp()
        
    }
    
    override func setUp() {
        super.setUp()
        
    }
    
    override class func tearDown() {
        super.tearDown()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInternetConnectivity() {
        XCTAssertTrue(Reachability.isConnectedToNetwork() == true)
    }
    
    func testFactAPIURL() {
        XCTAssertEqual(factURL, "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json")
    }
    
    // check for response
    
    func testResponseData() {
        guard let url = URL(string: factURL) else { return }
        
        let promise = expectation(description: "Get Request")
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else { return }
            do {
                guard let string = String(data: data, encoding: String.Encoding.isoLatin1) else { return }
                guard let properData = string.data(using: .utf8, allowLossyConversion: true) else { return }
                
                let json = try JSONSerialization.jsonObject(with: properData, options: JSONSerialization.ReadingOptions.mutableContainers)
                if let result = json as? NSDictionary {
                    XCTAssertTrue(result["title"]  != nil)
                    XCTAssertTrue(result["rows"]  != nil)
                    promise.fulfill()
                }
            } catch let error {
                print("Error found", error)
            }
        }.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testResponseFactList() {
        guard let url = URL(string: factURL) else { return }
        let promise = expectation(description: "Facts List")
        
        URLSession.shared.dataTask(with: url) { (data, response
            , error) in
            guard let data = data else { return }
            do {
                guard let string = String(data: data, encoding: String.Encoding.isoLatin1) else { return }
                guard let properData = string.data(using: .utf8, allowLossyConversion: true) else { return }
                
                let json = try JSONSerialization.jsonObject(with: properData, options: JSONSerialization.ReadingOptions.mutableContainers)
                
                if let result = json as? NSDictionary {
                    if let factList = result.object(forKey: "rows") as? NSArray {
                        if factList.count > 0 {
                            if let firstObj = factList.firstObject as? NSDictionary {
                                XCTAssertTrue(firstObj["title"]  != nil)
                                XCTAssertTrue(firstObj["description"]  != nil)
                                XCTAssertTrue(firstObj["imageHref"]  != nil)
                            }
                        }
                    }
                    promise.fulfill()
                }
            } catch let err {
                print("Error", err)
            }
        }.resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
}
