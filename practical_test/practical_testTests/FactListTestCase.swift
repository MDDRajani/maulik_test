//
//  FactListTestCase.swift
//  practical_testTests
//
//  Created by Maulik on 26/06/20.
//  Copyright © 2020 wipro. All rights reserved.
//

import XCTest

@testable import practical_test

class FactListTestCase: XCTestCase {

    var factListVC = FactsListVC()
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    override class func setUp() {
        super.setUp()
        
    }
    
    override func setUp() {
        super.setUp()
        
    }
    
    override class func tearDown() {
        super.tearDown()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testUseTableViewAsContainerView() {
        XCTAssertNotNil(factListVC.tableView)
    }
    
    func testTableViewConfromsToDelegate() {
        XCTAssertTrue(factListVC.conforms(to: UITableViewDelegate.self))
    }
    
    func testTableViewConformsToDataSource() {
        XCTAssertTrue(factListVC.responds(to: #selector(factListVC.tableView(_:numberOfRowsInSection:))))
        XCTAssertTrue(factListVC.responds(to: #selector(factListVC.tableView(_:cellForRowAt:))))
    }
    
    
    
}
