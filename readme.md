This app is fetching data from provided url and parse data using Decodable protocol.
As per instruction have to use 'UITableView' as main container.
Used 'Kingfisher' CocoaPods for downloading images from response.
Best attempt to achieve 'Modular coding' pattern.
Had made few comments as a part of general practice of mine.
Include possible test cases.

Things to remember to execute program:

 - Development Environment

  	1. Xcode Version - 11.5 (11E608c)) 
  	2. iOS Deployment Target - 13.5
   